#include "PluginFramework.h"

int main() {
    PluginFramework framework;
#ifdef __linux__
    framework.registerPlugin("./libhelloworld.so");
#elif _WIN32
    framework.registerPlugin("./helloworld.dll");
#endif	
    framework.runPlugin("HelloWorld");
    return 0;
}
