#include <string>
#include <map>
#include <iostream>

#include "Plugin.h"

class PluginFramework {
public:
    void registerPlugin(std::string pluginPath);
    void runPlugin(std::string name);

private:
    std::map<std::string, Plugin*> plugins;
};
