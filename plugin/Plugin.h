#include <string>

#ifdef __cplusplus
#ifdef __linux__
#define EXPORT_DLL extern "C"
#elif _WIN32
#define EXPORT_DLL extern "C" __declspec(dllexport)
#endif
#else
#define EXPORT_DLL __declspec(dllexport)
#endif

class Plugin {
public:
    virtual std::string getName() = 0;
    virtual void run() = 0;
};