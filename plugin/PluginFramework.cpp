#include "PluginFramework.h"
#include "Log.h"

#ifdef __linux__
#include <dlfcn.h>
#elif _WIN32
#include <windows.h>
#endif

void PluginFramework::registerPlugin(std::string pluginPath) {
#ifdef __linux__
	void* pluginHandle = dlopen(pluginPath.c_str(), RTLD_LAZY);
#elif _WIN32	
	HMODULE pluginHandle = LoadLibrary(pluginPath.c_str());
#endif
	if (pluginHandle == NULL) {
		LOG_ERROR("Error loading plugin ");
		return;
	}

	Plugin* (*createPlugin)();
#ifdef __linux__	
	createPlugin = (Plugin* (*)()) dlsym(pluginHandle, "createPlugin");
#elif _WIN32
	createPlugin = (Plugin* (*)())GetProcAddress(pluginHandle, "createPlugin");
#endif
	if (createPlugin == NULL) {
		LOG_ERROR("Error loading symbol");
#ifdef __linux__
		dlclose(pluginHandle);
#elif _WIN32
		FreeLibrary(pluginHandle);
#endif
		return;
	}

	Plugin* plugin = createPlugin();
	plugins[plugin->getName()] = plugin;
}

void PluginFramework::runPlugin(std::string name) {
	if (plugins.find(name) != plugins.end()) {
		plugins[name]->run();
	}
	else {
		LOG_ERROR("Plugin not found");
	}
}