#include "HelloWorldPlugin.h"
#include "Log.h"

Plugin* createPlugin() {
    return new HelloWorldPlugin;
}

std::string HelloWorldPlugin::getName() {
	return "HelloWorld";
}

void HelloWorldPlugin::run() {
	LOG_INFO("Hello, World!");
}