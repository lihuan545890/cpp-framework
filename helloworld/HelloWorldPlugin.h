#include "Plugin.h"
#include <iostream>

class HelloWorldPlugin : public Plugin {
public:
    std::string getName();
    void run();
};

EXPORT_DLL Plugin* createPlugin();


