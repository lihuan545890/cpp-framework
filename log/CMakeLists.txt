#1.cmake verson，指定cmake版本 
cmake_minimum_required(VERSION 3.2)

include_directories(
./
../plugin
)

file(GLOB_RECURSE DIR_HELLO_SRCS "*.h" "*.cpp")  
set(all_files ${DIR_HELLO_SRCS})
source_group_by_dir(all_files)

set(LIBRARY_OUTPUT_PATH  ${PROJECT_SOURCE_DIR}/build)
add_library(log SHARED ${DIR_HELLO_SRCS})
target_link_libraries(log)
