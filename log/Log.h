#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <stdbool.h>

//#include <pthread.h>

#ifdef __cplusplus
#ifdef __linux__
#define EXPORT_LOG_DLL
#elif _WIN32
#define EXPORT_LOG_DLL extern "C" __declspec(dllexport)
#endif
#else
#define EXPORT_LOG_DLL __declspec(dllexport)
#endif

#define LOG_LEVEL_DEBUG 0
#define LOG_LEVEL_INFO  1
#define LOG_LEVEL_WARN  2
#define LOG_LEVEL_ERROR 3

#define MAX_LOG_SIZE 1024

static const char* log_level_names[] = {
    "DEBUG", "INFO", "WARN", "ERROR"
};

typedef struct {
    int level;           // ��־����
    bool use_file;       // �Ƿ���־������ļ�
    const char* file;    // ��־�ļ���
    FILE* fp;            // �ļ�ָ��
    bool use_filter;     // �Ƿ����ù�����
    const char* filter;  // ��������֧��ͨ���
    //pthread_mutex_t mutex;
} logger_t;

static logger_t logger = {
    LOG_LEVEL_INFO,
    false,
    NULL,
    NULL,
    false,
    NULL
};

EXPORT_LOG_DLL int log_init(int level, bool use_file, const char* file, bool use_filter, const char* filter);
EXPORT_LOG_DLL void log_write(int level, const char* file, int line, const char* fmt, ...);
EXPORT_LOG_DLL int log_close();

#define LOG(level, fmt, ...) log_write(level, __FILE__, __LINE__, fmt, ##__VA_ARGS__)

#define LOG_DEBUG(fmt, ...) LOG(LOG_LEVEL_DEBUG, fmt, ##__VA_ARGS__)
#define LOG_INFO(fmt, ...)  LOG(LOG_LEVEL_INFO,  fmt, ##__VA_ARGS__)
#define LOG_WARN(fmt, ...) LOG(LOG_LEVEL_WARN, fmt, ##__VA_ARGS__)
#define LOG_ERROR(fmt, ...)  LOG(LOG_LEVEL_ERROR,  fmt, ##__VA_ARGS__)
