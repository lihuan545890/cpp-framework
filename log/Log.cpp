#include "Log.h"

void log_write(int level, const char* file, int line, const char* fmt, ...)
{
    char buf[MAX_LOG_SIZE];
    va_list args;

    // �����־����
    if (level < logger.level) {
        return;
    }

    // ��������
    if (logger.use_filter && strstr(file, logger.filter) == NULL) {
        return;
    }

    // ������־��Ϣ
    va_start(args, fmt);
    vsnprintf(buf, sizeof(buf), fmt, args);
    va_end(args);

    char timestamp[20] = {0};
#ifdef __linux__
    time_t now = time(NULL);
    struct tm tm_now;
    localtime_r(&now, &tm_now);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", &tm_now);
#elif _WIN32
    time_t now;
    time(&now);
    struct tm* tm_now = localtime(&now);
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_now);
#endif



    // ���������̨
    //pthread_mutex_lock(&logger.mutex);
    printf("[%s] %s:%d %s\n", timestamp, file, line, buf);
    //pthread_mutex_unlock(&logger.mutex);

    // ������ļ�
    if (logger.use_file) {
        //pthread_mutex_lock(&logger.mutex);
        if (logger.fp == NULL) {
            logger.fp = fopen(logger.file, "a");
        }
        if (logger.fp != NULL) {
            fprintf(logger.fp, "[%s] %s:%d %s\n", timestamp, file, line, buf);
            fflush(logger.fp);
        }
        //pthread_mutex_unlock(&logger.mutex);
    }
}

int log_init(int level, bool use_file, const char* file, bool use_filter, const char* filter)
{
    logger.level = level;
    logger.use_file = use_file;
    logger.use_filter = use_filter;
    logger.file = file;
    logger.filter = filter;
    //pthread_mutex_init(&logger.mutex, NULL);
    return 0;
}

int log_close()
{
    if (logger.fp != NULL) {
        fclose(logger.fp);
        logger.fp = NULL;
    }
    //pthread_mutex_destroy(&logger.mutex);
    return 0;
}